import {
    FETCH_PENDING_API,
    FETCH_SUCCESS_API,
    FETCH_ERROR_API,
    CONFIRM_CHANGE_STATUS_ORDER,
    SET_STATUS_EDIT_MODAL,
    CHANGE_STATUS_ORDER_HANDLER,
    BUTTON_EDIT_HANDLER,
    OPEN_TOAST,
    FETCH_API_VOUCHER,
    GET_INFO_CREATE_NEW_ORDER,
    GET_USER_CREATE_NEW_ORDER,
    BUTTON_CREATE_NEW_ORDER,
    SET_STATUS_CREATE_MODAL,
    FETCH_API_DRINK,
    BUTTON_DELETE_HANDLER,
    CHANGE_STATUS_DELETE,
    CONFIRM_DELETE_ORDER_HANDLER,
    ALERT_FORM_DELETE,
    CONFIRM_ALERT_FORM_DELETE,
    ALERT_FORM_CREATE,
    CONFIRM_ALERT_FORM_CREATE,
    ALERT_FORM_EDIT,
    CONFIRM_ALERT_FORM_EDIT,
} from "../constants/order.constants";

//action when clicked button create new order modal
export const createNewOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: FETCH_PENDING_API,
        });
        var infor = data
        const body = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({
                kichCo: infor.pizzaSize.kichCo,
                duongKinh: infor.pizzaSize.duongKinh,
                suon: infor.pizzaSize.suon,
                salad: infor.pizzaSize.salad,
                loaiPizza: infor.pizzaType,
                idVourcher: infor.userInfor.idVourcher,
                idLoaiNuocUong: infor.drink,
                soLuongNuoc: infor.pizzaSize.soLuongNuoc,
                hoTen: infor.userInfor.hoTen,
                thanhTien: infor.pizzaSize.thanhTien,
                email: infor.userInfor.email,
                soDienThoai: infor.userInfor.soDienThoai,
                diaChi: infor.userInfor.diaChi,
                loiNhan: infor.userInfor.loiNhan
            }),
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", body);
            const data = await response.json();

            return dispatch({
                type: BUTTON_CREATE_NEW_ORDER,
                data: data
            })

        } catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}
//thay đổi trạng thái đóng mở create modal
export const setStatusCreate = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: SET_STATUS_CREATE_MODAL,
            data: data
        })
    }
}
//action get information of user on create new order form
export const collectInforUserOnForm = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: GET_USER_CREATE_NEW_ORDER,
            data: data
        });
        await dispatch(fetchVoucherId(data.userInfor.voucherId))
    }
}
//action get information on create new order form
export const collectInforOnForm = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: GET_INFO_CREATE_NEW_ORDER,
            data: data
        });
    }
}
//call api check voucher id 
export const fetchVoucherId = (voucherId) => {
    return async (dispatch) => {
        await dispatch({
            type: FETCH_PENDING_API,
        })
        var body = {
            method: 'GET',
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + voucherId, body);
            const data = await response.json();
            return dispatch({
                type: FETCH_API_VOUCHER,
                data: data.discount
            })
        } catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}
//action call api drink 
export const fetchDrinkApi = () => {
    return async (dispatch) => {
        await dispatch({
            type: FETCH_PENDING_API
        });
        var body = {
            method: 'GET',
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", body);
            const data = await response.json();
            //nếu dữ liệu được lấy thành công truyền data drink đã lấy vào data
            return dispatch({
                type: FETCH_API_DRINK,
                data: data
            })
            //trường hợp lỗi
        } catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}
//action khi nút edit được bấm
export const editOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: BUTTON_EDIT_HANDLER,
            data: data,
        })
    }
}
//action khi thay đổi status order
export const changeStatusOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CHANGE_STATUS_ORDER_HANDLER,
            data: data
        })
    }
}

//thay đổi trạng thái đóng mở edit modal
export const setStatusEdit = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: SET_STATUS_EDIT_MODAL,
            data: data
        });
    }
}
//sửa order
export const confirmEditOrderHandler = (data, orderId) => {
    return async (dispatch) => {
        await dispatch({
            type: FETCH_PENDING_API
        });
        //khai báo body api POST
        const body = {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            redirect: 'follow',
            body: JSON.stringify({ trangThai: data }),

        };
        //Call POST api
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders/" + orderId, body);
            const data = await response.json();
            dispatch({
                type: CONFIRM_CHANGE_STATUS_ORDER,
                data: data
            })
        }
        catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}

//call api get all order for loading on table
export const callApiGetAllOrder = () => {
    return async (dispatch) => {
        const body = {
            method: 'GET',
            redirect: 'follow'
        };
        await dispatch({
            type: FETCH_PENDING_API
        });
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", body);
            const data = await response.json();
            dispatch({
                type: FETCH_SUCCESS_API,
                data: data
            })
            dispatch({
                type: OPEN_TOAST,
                data: {
                    open: "true",
                    message: "Loading All Order Successfully",
                    type: "success"
                }
            })
        }
        catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}
export const deleteOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: BUTTON_DELETE_HANDLER,
            data: data,
        })
    }
}
export const setStatusDelete = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CHANGE_STATUS_DELETE,
            data: data
        })
    }
}
export const confirmDeleteOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: FETCH_PENDING_API,
        });
        var body = {
            method: 'DELETE',
            redirect: 'follow'
        };
        var id = data
        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + id, body);
            dispatch({
                type: CONFIRM_DELETE_ORDER_HANDLER,
                response: response.message
            })
        } catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
        }
    }
}
export const setStatusAlertDelete = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: ALERT_FORM_DELETE,
            data: data
        })
    }
}
export const confirmAlertDelete = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CONFIRM_ALERT_FORM_DELETE,
            data: data
        })
    }
}
export const setStatusAlertCreate = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: ALERT_FORM_CREATE,
            data: data
        })
    }
}
export const confirmAlertCreate = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CONFIRM_ALERT_FORM_CREATE,
            data: data
        })
    }
}
export const setStatusAlertEdit = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: ALERT_FORM_EDIT,
            data: data
        })
    }
}
export const confirmAlertEdit = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CONFIRM_ALERT_FORM_EDIT,
            data: data
        })
    }
}
//action khi các sự kiện thành công
export const openToast = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: OPEN_TOAST,
            data: data
        })
    }
}