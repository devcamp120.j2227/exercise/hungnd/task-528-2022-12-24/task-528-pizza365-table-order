import { Button, CircularProgress, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { callApiGetAllOrder, deleteOrderHandler, editOrderHandler, fetchDrinkApi, setStatusCreate, setStatusDelete, setStatusEdit } from "../actions/order.action";
import AlertFormCreate from "./alertForm/alertCreateOrder";
import AlertFormDelte from "./alertForm/alertDelete";
import AlertFormEdit from "./alertForm/alertEditOrder";
import AddOrderModal from "./modal/addOrderModal";
import DeleteOrderModal from "./modal/deleteOrderModal";
import EditOrderModal from "./modal/editOrderModal";

const OrderTable = () => {
    const dispatch = useDispatch();
    const { orders, pending, totalOrder, openEdit, statusCreate, statusDelete, openAlertDelete, openAlertCreate, openAlertEdit } = useSelector((reduxUser) =>
        reduxUser.orderReducer
    )
    const [page, setPage] = useState(0);

    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    //sử dụng use effect để load toàn bộ thông tin order vào table
    useEffect(() => {
        dispatch(callApiGetAllOrder());
        dispatch(fetchDrinkApi());
    }, []);
    const onBtnEditHandler = (value) => {
        dispatch(editOrderHandler(value));
        dispatch(setStatusEdit(true))
    }
    const onBtnDeleteHandler = (value) => {
        dispatch(deleteOrderHandler(value));
        dispatch(setStatusDelete(true));
    }
    const onBtnAddOrderHandler = () => {
        dispatch(setStatusCreate(true));
    };

    return (
        <Container>
            <Grid container mt={5}>
                <Grid
                    container
                    justifyContent="center"
                >
                    <h2>ORDER INFO</h2>
                </Grid>
                <Grid xs={12}>
                    <Button variant="contained" style={{ backgroundColor: "green" }} onClick={onBtnAddOrderHandler}><i class="fas fa-plus"></i>&nbsp;Add Order</Button>
                </Grid>
                {statusCreate === true ? <AddOrderModal /> : null}
                {openEdit === true ? <EditOrderModal /> : null}
                {statusDelete === true ? <DeleteOrderModal /> : null}
                {openAlertCreate === true ? <AlertFormCreate /> : null}
                {openAlertDelete === true ? <AlertFormDelte /> : null}
                {openAlertEdit === true ? <AlertFormEdit /> : null}
                <Grid item md={12} sm={12} lg={12} xs={12} mt={2}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead className="header-table">
                                <TableRow >
                                    <TableCell id="header-detail">Order ID</TableCell>
                                    <TableCell id="header-detail">Pizza Size</TableCell>
                                    <TableCell id="header-detail">Pizza Type</TableCell>
                                    <TableCell id="header-detail">Drink</TableCell>
                                    <TableCell id="header-detail">Price</TableCell>
                                    <TableCell id="header-detail">Customer Name</TableCell>
                                    <TableCell id="header-detail">Phone Number</TableCell>
                                    <TableCell id="header-detail">Order Status</TableCell>
                                    <TableCell id="header-detail">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            {pending ?
                                <TableBody>
                                    <TableRow>
                                        <TableCell><CircularProgress /></TableCell>
                                    </TableRow>
                                </TableBody>
                                : <TableBody>
                                    {orders.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((value, index) => {
                                            return (
                                                <TableRow hover key={index} lg={12} md={12} sm={12} xs={12}>
                                                    <TableCell id="body-table-text">{value.orderCode}</TableCell>
                                                    <TableCell id="body-table-text">{value.kichCo}</TableCell>
                                                    <TableCell id="body-table-text">{value.loaiPizza}</TableCell>
                                                    <TableCell id="body-table-text">{value.idLoaiNuocUong}</TableCell>
                                                    <TableCell id="body-table-text">{value.thanhTien}</TableCell>
                                                    <TableCell id="body-table-text">{value.hoTen}</TableCell>
                                                    <TableCell id="body-table-text">{value.soDienThoai}</TableCell>
                                                    <TableCell id="body-table-text">{value.trangThai}</TableCell>
                                                    <TableCell id="body-table-text">
                                                        <Grid container columns={12} style={{ width: 100 }}>
                                                            <Grid xs={6} pl={2}>
                                                                <Button id="btn-edit" onClick={() => onBtnEditHandler(value)}>
                                                                    <span style={{ color: "green" }}><i class="fas fa-edit fa-2x"></i></span>
                                                                </Button>
                                                            </Grid>
                                                            <Grid xs={6} pl={2}>
                                                                <Button id="btn-del" onClick={() => onBtnDeleteHandler(value)}>
                                                                    <span style={{ color: "red" }}><i class="far fa-trash-alt fa-2x"></i></span>
                                                                </Button>
                                                            </Grid>
                                                        </Grid>
                                                    </TableCell>
                                                </TableRow >
                                            )
                                        })
                                    }
                                </TableBody>}
                        </Table>
                        <TablePagination
                            rowsPerPageOptions={[10, 25, 50, 100]}
                            component="div"
                            count={totalOrder}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </TableContainer>
                </Grid>
            </Grid>
        </Container>
    )
}

export default OrderTable;