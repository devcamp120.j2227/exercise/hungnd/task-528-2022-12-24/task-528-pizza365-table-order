import { Grid, Button, Box, Typography, Modal } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { confirmDeleteOrderHandler, setStatusAlertDelete, setStatusDelete } from '../../actions/order.action';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
export default function DeleteOrderModal() {
    const { statusDelete, orderInfor} = useSelector((reduxData) => reduxData.orderReducer);
    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch(setStatusDelete(false))
    }
    const onBtnDeleteOrderHandler = () => {
        dispatch(confirmDeleteOrderHandler(orderInfor.id))
        dispatch(setStatusDelete(false));
        dispatch(setStatusAlertDelete(true));
    }
    return (
        <div>
            <Modal
                open={statusDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Delete Order
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <Grid container spacing={2}>
                            <Grid item>
                                <p>Bạn có chắc muốn xóa order có id {orderInfor.id}</p>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Grid container mt={2} spacing={2}
                        direction="row"
                        justifyContent="flex-end"
                        alignItems="center">
                        <Grid item>
                            <Button onClick={handleClose} variant="outlined">Close</Button>
                        </Grid>
                        <Grid item>
                            <Button onClick={onBtnDeleteOrderHandler} variant="contained" style={{ backgroundColor: "red" }}> Delete Order</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </div>
    );
}