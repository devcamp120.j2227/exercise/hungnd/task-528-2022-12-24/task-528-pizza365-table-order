
import { useDispatch, useSelector } from 'react-redux';
import { Grid, TextField, Select, MenuItem, Button, Box, Typography, Modal } from '@mui/material';
import { callApiGetAllOrder, confirmEditOrderHandler, setStatusEdit, changeStatusOrderHandler, setStatusAlertEdit } from "../../actions/order.action";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 1400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4
};

export default function EditOrderModal() {
  const dispatch = useDispatch();
  const { orderInfor, openEdit, statusOrder } = useSelector((reduxData) =>
    reduxData.orderReducer
  )
  const handleChange = (event) => {
    dispatch(changeStatusOrderHandler(event.target.value));
  }
  const onBtnEditOrderHandler = () => {
    dispatch(confirmEditOrderHandler(statusOrder, orderInfor.id));
    dispatch(callApiGetAllOrder());
    dispatch(setStatusAlertEdit(true));

  }
  const handleClose = () => {
    dispatch(setStatusEdit(false));
  }
  return (
    <div>
      <Modal
        open={openEdit}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        scroll="paper"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
          >
            <h1>Edit Order</h1>
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 5 }}>
            <Grid container spacing={2}>
              <Grid xs={6} pr={2}>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Order ID(Mã đơn hàng):</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="orderCode" value={`${orderInfor.orderCode}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>                       
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Có Combo:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="kichCo" value={`${orderInfor.kichCo}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Đường kính Pizza:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="duongKinh" value={`${orderInfor.duongKinh}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Sườn nướng:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="suon" value={`${orderInfor.suon}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Đồ uống:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="doUong" value={`${orderInfor.idLoaiNuocUong}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Số lượng nước uống:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="soLuongNuoc" value={`${orderInfor.soLuongNuoc}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>VoucherID:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="idVoucher" value={`${orderInfor.idVourcher}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Loại Pizza:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="loaiPizza" value={`${orderInfor.loaiPizza}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Salad:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="salad" value={`${orderInfor.salad}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Thành tiền:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="thanhTien" value={`${orderInfor.thanhTien} VND`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
              </Grid>
              <Grid xs={6} style={{ paddingLeft: '20px', borderLeft: "2px solid black" }}>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Giảm giá:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="giamGia" value={`${orderInfor.giamGia} VND`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Họ và tên:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="hoVaTen" value={`${orderInfor.hoTen}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Email:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="email" value={`${orderInfor.email}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Số điện thoại:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="soDienThoai" value={`${orderInfor.soDienThoai}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Địa chỉ:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="diaChi" value={`${orderInfor.diaChi}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Lời nhắn:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="loiNhan" value={`${orderInfor.loiNhan}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Trạng thái đơn hàng:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <Select
                      fullWidth
                      size="small"
                      name="trangThai"
                      value={statusOrder}
                      onChange={handleChange}
                    >
                      <MenuItem value="open">Open</MenuItem>
                      <MenuItem value="confirmed">Confirmred</MenuItem>
                      <MenuItem value="cancel">Cancel</MenuItem>
                    </Select>
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Ngày tạo đơn:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="ngayTao" value={`${orderInfor.ngayTao}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5} >
                    <p>Ngày cập nhật:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="ngayCapNhat" value={`${orderInfor.ngayCapNhat}`} variant="outlined" fullWidth size="small" disabled />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Typography>
          <Grid container mt={2} spacing={2}
            direction="row"
            justifyContent="flex-end"
            alignItems="center">
            <Grid item>
              <Button onClick={handleClose} variant="outlined">Close</Button>
            </Grid>
            <Grid item>
              <Button onClick={onBtnEditOrderHandler} variant="contained"> Edit Order</Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}