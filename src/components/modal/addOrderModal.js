import { Box, Typography, Grid, FormControl, InputLabel, Select, MenuItem, TextField, Button, Modal } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { collectInforOnForm, callApiGetAllOrder, collectInforUserOnForm, createNewOrderHandler, fetchVoucherId, setStatusCreate, setStatusAlertCreate } from "../../actions/order.action";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 1400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4
};

export default function AddOrderModal() {

  const dispatch = useDispatch();
  const {
    statusCreate,
    newOrder, size, pizzaType, pizzaSize, drink,
    suonNuong, salad, soLuongNuoc, price, newPrice
  } = useSelector((reduxData) =>
    reduxData.orderReducer
  )
  const [newUser, getNewOrder] = useState({
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    idVourcher: "",
    loiNhan: ""
  })
  const handleChange = (event) => {
    dispatch(collectInforOnForm({
      [event.target.name]: event.target.value,
    }))
  }
  const handleUserChange = (event) => {
    getNewOrder({
      ...newUser,
      [event.target.name]: event.target.value
    });
  }
  const onBtnCreateHandler = () => {
    if (!pizzaSize) {
      alert("Vui lòng chọn size pizza!");
      return;
    }
    if (!pizzaType) {
      alert("Vui lòng chọn pizza type!");
      return;
    }
    if (!newOrder.drink) {
      alert("Vui lòng chọn drink!");
      return;
    }
    if (!newUser.hoTen) {
      alert("Vui lòng nhập họ tên!");
      return;
    }
    if (newUser.email !== "") {
      if (validateEmail(newUser.email) === false) {
        alert("Chưa đúng định dạng email!");
        return;
      }
    }
    if (!newUser.soDienThoai) {
      alert("Vui lòng nhập số điện thoại!");
      return;
    }
    if (validatePhone(newUser.soDienThoai) === false) {
      alert("Số điện thoại không đúng dịnh dạng!");
      return;
    }
    if (!newUser.diaChi) {
      alert("Vui lòng nhập địa chỉ!");
      return;
    }
    dispatch(collectInforUserOnForm(newUser)); //1 đẩy thông tin user vào reduce 
    dispatch(fetchVoucherId(newUser.idVourcher));   //2 call api check voucher
    dispatch(createNewOrderHandler(newOrder));  //3 trả về được toàn bộ thông tin cần để POST
    dispatch(callApiGetAllOrder());
    dispatch(setStatusAlertCreate(true));
  }
  const handleClose = () => {
    dispatch(setStatusCreate(false));
  }
  const validateEmail = (emailform) => {
    var atposition = emailform.indexOf("@");
    var dotposition = emailform.lastIndexOf(".");
    if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailform.length) {
      return false;
    }
    return true;
  }
  const validatePhone = (paramPhoneNumber) => {
    var vform = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vform.test(paramPhoneNumber)) {
      return true;
    }
    else {
      return false;
    }
  }
  return (
    <div>
      <Modal
        open={statusCreate}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        scroll="paper"
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            textAlign="center"
          >
            <h1> Create New Order</h1>
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 5 }}>
            <Grid container spacing={2}>
              <Grid xs={6}>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Pizza Size:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <Box sx={{ minWidth: 120 }}>
                      <FormControl fullWidth size="small">
                        <InputLabel>Select Size</InputLabel>
                        <Select
                          value={pizzaSize}
                          name="pizzaSize"
                          label="Select Size"
                          onChange={handleChange}
                          required
                        >
                          <MenuItem value="S">S</MenuItem>
                          <MenuItem value="M">M</MenuItem>
                          <MenuItem value="L">L</MenuItem>
                        </Select>
                      </FormControl>
                    </Box>
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Đường Kính:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField
                      value={size}
                      variant="filled"
                      fullWidth
                      size="small"
                      disabled
                      InputProps={{
                        disableUnderline: true, style: { paddingBottom: "10px", height: "40px", borderRadius: "5px" }
                      }} />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Sườn nướng:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField
                      value={suonNuong}
                      variant="filled"
                      fullWidth size="small"
                      disabled
                      InputProps={{
                        disableUnderline: true, style: { paddingBottom: "10px", height: "40px", borderRadius: "5px" }
                      }} />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Salad:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField
                      value={salad}
                      variant="filled"
                      fullWidth size="small"
                      disabled
                      InputProps={{
                        disableUnderline: true, style: { paddingBottom: "10px", height: "40px", borderRadius: "5px" }
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Số Lượng Nước:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField
                      value={soLuongNuoc}
                      variant="filled"
                      fullWidth size="small"
                      disabled
                      InputProps={{
                        disableUnderline: true, style: { paddingBottom: "10px", height: "40px", borderRadius: "5px" }
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Thành Tiền:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField
                      value={newPrice === 0 ? price : newPrice}
                      variant="filled"
                      fullWidth size="small"
                      disabled
                      InputProps={{
                        disableUnderline: true, style: { paddingBottom: "10px", height: "40px", borderRadius: "5px" }
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Pizza Type:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <Box sx={{ minWidth: 120 }}>
                      <FormControl fullWidth size="small">
                        <InputLabel>Select Pizza Type</InputLabel>
                        <Select
                          value={pizzaType}
                          label="Select Pizza Type"
                          name="pizzaType"
                          onChange={handleChange}
                          required
                        >
                          <MenuItem value="Seafood">Hải sản</MenuItem>
                          <MenuItem value="Hawaii">Hawaii</MenuItem>
                          <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                        </Select>
                      </FormControl>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid xs={6} pl={2}>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Drink:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <Box sx={{ minWidth: 120 }}>
                      <FormControl fullWidth size="small">
                        <InputLabel>Select Drink</InputLabel>
                        <Select
                          value={newOrder.drink}
                          label="Select Drink"
                          name="drink"
                          onChange={handleChange}
                          required
                        >
                          {drink.map((value, index) => {
                            return (
                              <MenuItem value={value.maNuocUong} key={index}>{value.tenNuocUong}</MenuItem>
                            )
                          })}
                        </Select>
                      </FormControl>
                    </Box>
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Họ Tên:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="hoTen" value={newUser.hoTen} variant="outlined" fullWidth size="small" onChange={handleUserChange} required />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Số Điện Thoại:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="soDienThoai" type="number" value={newUser.soDienThoai} variant="outlined" fullWidth size="small" onChange={handleUserChange} required />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Email:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField required name="email" type="email" value={newUser.email} variant="outlined" fullWidth size="small" onChange={handleUserChange} />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Địa Chỉ:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="diaChi" value={newUser.diaChi} variant="outlined" fullWidth size="small" onChange={handleUserChange} required />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Mã voucher:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="idVourcher" value={newUser.idVourcher} variant="outlined" fullWidth size="small" onChange={handleUserChange} required />
                  </Grid>
                </Grid>
                <Grid container columns={16}>
                  <Grid item xs={5}>
                    <p>Lời Nhắn:</p>
                  </Grid>
                  <Grid item xs={11} >
                    <TextField name="loiNhan" value={newUser.loiNhan} variant="outlined" fullWidth size="small" onChange={handleUserChange} required />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Typography>
          <Grid container mt={2} spacing={2}
            direction="row"
            justifyContent="flex-end"
            alignItems="center">
            <Grid item>
              <Button onClick={handleClose} variant="outlined">Close</Button>
            </Grid>
            <Grid item>
              <Button onClick={onBtnCreateHandler} variant="contained"> Create Order</Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  )
}