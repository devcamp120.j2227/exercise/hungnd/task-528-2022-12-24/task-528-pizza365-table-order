import { Modal, Box, Typography, Grid, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { confirmAlertDelete } from "../../actions/order.action";

const style = {
    position: 'absolute',
    marginTop: '130px',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: '#F8EFBA',
    border: '2px solid #000',
    borderRadius: '5px',
    boxShadow: 24,
    p: 4,
};

export default function AlertFormDelte() {
    const { openAlertDelete } = useSelector((reduxData) => reduxData.orderReducer);
    const dispatch = useDispatch();
    const onBtnConfirmForm = () => {
        dispatch(confirmAlertDelete(false));
    }
    return (
        <div>
            <Modal
                open={openAlertDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <Grid container spacing={2}>
                            <Grid item>
                                <p>Delete order successfully!</p>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Grid container mt={2} spacing={2}
                        direction="row"
                        justifyContent="flex-end"
                        alignItems="center">
                        <Grid item>
                            <Button onClick={onBtnConfirmForm} variant="contained">Xác Nhận</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </div>
    )
}