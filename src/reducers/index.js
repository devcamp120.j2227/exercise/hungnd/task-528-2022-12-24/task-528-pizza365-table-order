import { combineReducers } from "redux";
import orderReducer from "./order.reducer";
const rootReducer = combineReducers({
    orderReducer
});
export default rootReducer;